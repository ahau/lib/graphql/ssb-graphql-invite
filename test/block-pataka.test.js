const tape = require('tape')
const gql = require('graphql-tag')
const { promisify: p } = require('util')

const TestBot = require('./test-bot')

function Run (t) {
  return function run (msg, promise) {
    return promise
      .then(res => {
        if (res.error || res.error) {
          console.log('error response found')
          return res
        }

        t.pass(msg)
        if (res.data) {
          return Object.entries(res.data)[0][1]
        }
        return res
      })
      .catch(err => {
        console.error(msg, err)
        t.error(err, msg)
      })
  }
}

tape('blockPataka', async t => {
  const pataka = await TestBot()
  const alice = await TestBot()
  const bob = await TestBot()

  t.teardown(() => {
    pataka.ssb.close(true)
    alice.ssb.close(true)
    bob.ssb.close(true)
  })

  const inviteCode = await Run(t)(
    'create invite code',
    pataka.apollo.mutate({
      mutation: gql`mutation createInvite ($input: createInviteInput) {
        createInvite(input: $input)
      }`,
      variables: {
        input: {
          uses: 3,
          note: 'My note'
        }
      }
    })
  )

  const run = Run(t)

  await run(
    'alice uses pataka invite',
    alice.apollo.mutate({
      mutation: gql`mutation($inviteCode: String!) {
        acceptInvite(inviteCode: $inviteCode)
      }`,
      variables: {
        inviteCode
      }
    })
  )

  const blockId = await run(
    'alice blocks pataka',
    alice.apollo.mutate({
      mutation: gql`mutation($id: String!) {
        blockPataka(feedId: $id)
      }`,
      variables: {
        id: pataka.ssb.id
      }
    })
  )

  const blockContent = await p(alice.ssb.get)({ id: blockId, private: true })
    .then(val => val.content)
    .catch(t.fail)

  t.deepEqual(
    blockContent,
    {
      type: 'contact',
      contact: pataka.ssb.id,
      blocking: true,
      recps: [alice.ssb.id]
    },
    'alice blocked the pataka privately'
  )
  t.true(
    await p(alice.ssb.friends.isBlocking)({ source: alice.ssb.id, dest: pataka.ssb.id }),
    'alice is blocking the pataka (ssb-friends)'
  )

  await alice.apollo.mutate({
    mutation: gql`mutation($id: String!) {
      blockPataka(feedId: $id)
    }`,
    variables: {
      id: bob.ssb.id
    }
  })
    .then(() => t.fail('alice should be stopeed'))
    .catch(_ => t.pass('alice is stopped from using blockPataka on a peer'))

  await run(
    'alice unblocks pataka',
    alice.apollo.mutate({
      mutation: gql`mutation($id: String!) {
        unblockPataka(feedId: $id)
      }`,
      variables: {
        id: pataka.ssb.id
      }
    })
  )

  await p(setTimeout)(500)

  t.false(
    await p(alice.ssb.friends.isBlocking)({ source: alice.ssb.id, dest: pataka.ssb.id }),
    'alice is no longer blocking pataka (ssb-friends)'
  )

  await p(setTimeout)(100)
  // HACK - stops an error
  t.end()
})
