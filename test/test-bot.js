import AhauClient from 'ahau-graphql-client'

const crypto = require('crypto')
const Server = createSbot
const ahauServer = require('ahau-graphql-server')
const fetch = require('node-fetch')

const caps = { shs: crypto.randomBytes(32) }
let i = 0

module.exports = async function (opts = {}) {
  const stack = Server
    .use(require('ssb-db2/core'))
    .use(require('ssb-classic'))
    .use(require('ssb-db2/compat/publish'))
    .use(require('ssb-db2/compat/db'))
    .use(require('ssb-db2/compat/feedstate'))
    .use(require('ssb-db2/compat/history-stream'))
    .use(require('ssb-db2/compat/log-stream'))
    .use(require('ssb-db2/compat/ebt'))

    .use(require('ssb-conn'))
    .use(require('ssb-replicate')) // must be above friends
    .use(require('ssb-friends'))

    .use(require('ssb-profile'))
    .use(require('ssb-invite'))
    .use(require('ssb-blobs'))

    .use(require('ssb-box2'))
    .use(require('ssb-tribes'))

  const ssb = stack({
    // need different names so set up in different directories
    name: `ssb-graphql-invite-${Date.now()}-${i++}`,
    // fixes error: no address available for creating an invite, configuration needed for server.
    allowPrivate: true,
    // (mix) : don't know what allowPrivate does

    caps, // need to be on same caps to connect

    host: '127.0.0.1',
    port: 8000 + Math.floor(Math.random * 1000),
    // need different ports on this test computer to be able to connect
    ...opts,
    noDefaultUse: true,
    box2: {
      ...opts.box2,
      legacyMode: true
    }
  })

  const port = 3000 + Math.random() * 7000 | 0
  const httpServer = await ahauServer({
    schemas: [
      require('@ssb-graphql/main')(ssb),
      require('@ssb-graphql/profile')(ssb),
      require('..')(ssb)
    ],
    context: {},
    port
  })
  ssb.close.hook((close, [cb]) => {
    httpServer.close()
    close(cb)
  })

  const apollo = new AhauClient(port, { isTesting: true, fetch })

  return {
    ssb,
    apollo
  }
}

// NOTE scuttle-testbot default includes ssb-conn@6, which we don't currently support
// so we copied this and installed our own version of ssb-conn@1 for the moment

const ssbKeys = require('ssb-keys')
const { join } = require('path')
const rimraf = require('rimraf')
const os = require('os')

let plugins = []

function createSbot (opts = {}) {
  if (!opts.name) {
    opts.name = `ssb-test-${Date.now()}-${Math.floor(Math.random() * 1000)}`
  }
  if (!opts.path) {
    opts.path = join(os.tmpdir(), opts.name)
  }
  if (!opts.startUnclean) { rimraf.sync(opts.path) }
  if (!opts.keys) { opts.keys = ssbKeys.generate() }

  if (!opts.conn) opts.conn = {}
  if (typeof opts.conn.autostart !== 'boolean') opts.conn.autostart = false

  const caps = {
    shs: (opts.caps && opts.caps.shs) || crypto.randomBytes(32).toString('base64')
  }
  let createSbot = require('secret-stack')({ caps })
  //   .use(require('ssb-conn')) << DIFF here

  if (opts.db2) createSbot.use(require('ssb-db2'))
  else createSbot.use(require('ssb-db'))

  if (createSbot.createSbot) { createSbot = createSbot.createSbot() }
  plugins.forEach(plugin => createSbot.use(plugin))
  plugins = []

  return createSbot(opts)
}
createSbot.use = function use (plugin) {
  plugins.push(plugin)
  return createSbot
}
