const tape = require('tape')
const TestBot = require('./test-bot')

tape('create + accept invite', async t => {
  t.plan(8)
  const pataka = await TestBot()
  const ahau = await TestBot()

  log({
    pataka: pataka.ssb.id,
    ahau: ahau.ssb.id
  })

  const res1 = await pataka.apollo.mutate({
    mutation: `mutation createInvite ($input: createInviteInput) {
      createInvite(input: $input)
    }`,
    variables: {
      input: {
        uses: 3,
        note: 'My note'
      }
    }
  })
  t.error(res1.errors, 'query should not return errors')
  t.equals(typeof res1.data, 'object', 'res1.data is an object')
  t.equals(
    typeof res1.data.createInvite,
    'string',
    'res1.data.createInvite is a string'
  )

  const inviteCode = res1.data.createInvite
  console.log({ inviteCode })
  // NOTE - the pataka feedId embedded in the inviteCode, this is good

  const res2 = await ahau.apollo.mutate({
    mutation: `mutation($inviteCode: String!) {
      acceptInvite(inviteCode: $inviteCode)
    }`,
    variables: {
      inviteCode
    }
  })

  t.error(res2.errors, 'query should not return errors')
  t.equals(typeof res2.data, 'object', 'res2.data is an object')
  t.equals(
    res2.data.acceptInvite,
    pataka.ssb.id,
    'acceptInvite return sthe feedId of the pataka'
  )

  // NOTE - we get a weird error about not being able to replciate with a feedId
  // but this is ok - in ssb-replicate there's an sbot.on('rpc:connect') hook
  // which immediately tries to call createHistoryStream but in the case of an invite
  // it's a temp feedId (keypair) for a one-off connection ... so error is safe

  await new Promise(resolve => setTimeout(resolve, 100))
  ahau.ssb.close(t.error)
  pataka.ssb.close(t.error)
})

function log (obj) {
  console.log('')
  console.log(JSON.stringify(obj, null, 2))
  console.log('')
}
