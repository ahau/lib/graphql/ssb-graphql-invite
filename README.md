# GraphQL for Secure Scuttlebutt

Invites GraphQL types and resolvers for Secure Scuttlebutt.
It provides primitives for creating and accepting invites.

## Usage

`npm i -S @ssb-graphql/invite`

## Example Usage

```js
const { ApolloServer } = require('apollo-server-express')
const { buildFederatedSchema } = require('@apollo/federation');

const Server = require('ssb-server')
const Config = require('ssb-config/inject')

const config = Config({})

const sbot = Server
  .use(require('ssb-invite')) // << required
  .call(null, config)

const invite = require('@ssb-graphql/invite')(sbot)

const server = new ApolloServer({
  schema: buildFederatedSchema([
    { typeDefs: invite.typeDefs, resolvers: invite.resolvers },
    // add other types + resolvers here!
  ])
})
```

## Requirements

An `ssb-server` / `secret-stack` with plugins: 
- `ssb-invite`
- `ssb-friends`

## API

See `/src/typeDefs.js` for the most up to date details on what's offered by this module.

## Testing

run `npm test` to run tests

